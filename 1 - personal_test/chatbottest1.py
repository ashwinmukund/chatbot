from keras.models import Sequential
from keras.layers import Dense, Activation

import pandas as pd
import csv

tick = True
def dataextraction():
    '''
    extracts questions and their repective answers an indexes from given excel sheet
    appends to individual lists
    dictionary sorts each question to respective answer
    prints dictionary, list of question, answers, and indexes on text file
    '''
    wb = pd.read_excel('datatosplurge.xlsx')
    rows = wb.shape[0]
    col = wb.shape[1]
    index = []
    question = []
    answer = []
    for i in range(rows):
        index.append(wb.iloc[i,0])
        question.append(str(wb.iloc[i,1]))
        answer.append(str(wb.iloc[i,2]))
        i += 1
    totaldata = dict(zip(question, answer))
    outT = open("totaldata.txt", "w")
    for i in totaldata:
        outT.write("Q: " + i + "\n")
        outT.write("A: " + totaldata[i] + "\n\n\n")
    outT.close()
    outQ = open("questions.txt", "w")
    for x in question:
        outQ.write(str(x))
        outQ.write("\n")
    outQ.close()
    outA = open("answers.txt", "w")
    for x in answer:
        outA.write(str(x))
        outA.write("\n")
    outA.close()
    outI = open("index.txt", "w")
    for x in index:
        outI.write(str(x))
        outI.write("\n")
    outI.close()
    return totaldata

def firstinput(givendata):
    questiontoask = input("Hello there! How can I help you today?:\n")
    while questiontoask != 'stop':
        return "Here's what you can do:\n\n" + givendata[questiontoask]
    if questiontoask == 'stop':
        global tick
        tick = False
        return "Thank you using the chatbot!"

def secondinput(givendata):
    questiontoask = input("\n\nWhat else would you like assistance with?:\n")
    while questiontoask != 'stop':
        return "Here's what you can do:\n\n" + givendata[questiontoask]
    if questiontoask == 'stop':
        global tick
        tick = False
        return "Thank you using the chatbot!"


def main():
    print(firstinput(dataextraction()))
    while tick == True:
        print(secondinput(dataextraction()))

if __name__ == "__main__":
    main()
