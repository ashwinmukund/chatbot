from keras.models import Sequential
from keras.layers import Dense, Activation

import pandas as pd
import csv

def main():
    wb = pd.read_excel('datatosplurge.xlsx')
    rows = wb.shape[0]
    col = wb.shape[1]
    index = []
    question = []
    answer = []
    for i in range(rows):
        index.append(wb.iloc[i,0])
        question.append(wb.iloc[i,1])
        answer.append(wb.iloc[i,2])
        i += 1

    with open("results.csv", 'wb') as resultFile:
        wr = csv.writer(resultFile, dialect='excel')
        #wr.writerows(index)
        wr.writerows(question)
        #wr.writerows(answer)

if __name__ == "__main__":
    main()
