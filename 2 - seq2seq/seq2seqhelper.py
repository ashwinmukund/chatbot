from keras.preprocessing.text import text_to_word_sequence
from keras.models import Sequential
from keras.layers import Dense, Activation, TimeDistributed,RepeatVector,recurrent,Embedding
from keras.layers.recurrent import LSTM
from keras.optimizers import Adam, RMSprop
from nltk import FreqDist
import numpy as np
import os
import datetime

import pandas as pd
def load_data(max_len, vocab_size):
    wb = pd.read_excel('testdata.xlsx',skiprows=1)
    rows = wb.shape[0]
    col = wb.shape[1]
    question = []
    var1 = ' '
    var2 = ' '
    answer = []
    #reading in the excel data and converting the data into a usable format
    for i in range(rows): 
        #this check is to ensure that we only read in actual questions and answers into our 2 sequences
        #since we have blank spaces within the answer and question columns in the excel document
        var1 = wb.iloc[i,0] 
        if(type(var1) == str): 
            question.append(text_to_word_sequence(var1)[::-1])
        var2 = wb.iloc[i,1] 
        if(type(var2) == str): 
            answer.append(text_to_word_sequence(var2)[::-1])
        i += 1
    #creating vocab set with most common words from both question and answers
    dist = FreqDist(np.hstack(question))
    question_vocab = dist.most_common(vocab_size-1)
    dist = FreqDist(np.hstack(answer))
    answer_vocab = dist.most_common(vocab_size-1)
    #creating an array of words from the vocabulary set, to be used as an index to word dictionary
    question_ix_to_word = [word[0] for word in question_vocab]
    # adding the word ZERO to the beginning of the array 
    question_ix_to_word.insert(0, 'ZERO')
    # adding the word UNK for unknown to the end of the array
    question_ix_to_word.append('UNK')

    question_word_to_ix = {word:ix for ix, word in enumerate(question_ix_to_word)}

    #converting each word to its index value
    for i, sentence in enumerate(question):
        for j, word in enumerate(sentence):
            if word in question_word_to_ix:
                question[i][j] = question_word_to_ix[word]
            else:
                question[i][j] = question_word_to_ix['UNK']
    answer_ix_to_word = [word[0] for word in answer_vocab]
    answer_ix_to_word.insert(0, 'ZERO')
    answer_ix_to_word.append('UNK')
    answer_word_to_ix = {word:ix for ix, word in enumerate(answer_ix_to_word)}
    for i, sentence in enumerate(answer):
        for j, word in enumerate(sentence):
            if word in question_word_to_ix:
                answer[i][j] = answer_word_to_ix[word]
            else:
                answer[i][j] = answer_word_to_ix['UNK']
    return(question, len(question_vocab)+2, question_word_to_ix, question_ix_to_word, answer, len(answer_vocab)+2, answer_word_to_ix,answer_ix_to_word)

def load_test_data(source, X_word_to_ix, max_len):
    f = open(source, 'r')
    X_data = f.read()
    f.close()

    X = [text_to_word_sequence(x)[::-1] for x in X_data.split('\n') if len(x) > 0 and len(x) <= max_len]
    for i, sentence in enumerate(X):
        for j, word in enumerate(sentence):
            if word in X_word_to_ix:
                X[i][j] = X_word_to_ix[word]
            else:
                X[i][j] = X_word_to_ix['UNK']
    return X

def create_model(X_vocab_len,X_max_len,y_vocab_len,y_max_len,hidden_size, num_layers):
    model = Sequential()
    #creating encoder network
    model.add(Embedding(X_vocab_len,1000,input_length=X_max_len,mask_zero=True))
    model.add(LSTM(hidden_size))
    model.add(RepeatVector(y_max_len))

    #creating decoder network
    for _ in range(num_layers):
        model.add(LSTM(hidden_size, return_sequences=True))
        model.add(TimeDistributed(Dense(y_vocab_len)))
        model.compile(loss='categorical_crossentropy',optimizer='rmsprop',metrics=['accuracy'])
    return model

def process_data(word_sentences,max_len,word_to_ix):
    sequences = np.zeros((len(word_sentences),max_len,len(word_to_ix)))
    for i, sentence in enumerate(word_sentences):
        for j,word in enumerate(sentence):
            sequences[i,j,word] = 1.
    return sequences
def find_checkpoint_file(folder):
    checkpoint_file = [f for f in os.listdir(folder) if 'checkpoint' in f]
    if len(checkpoint_file) == 0:
        return[]
    modified_time = [os.path.getmtime(f) for f in checkpoint_file]
    return checkpoint_file[np.argmax(modified_time)]
    

