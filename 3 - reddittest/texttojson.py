import csv
import json
import pandas as pd

def dataextraction():
    '''
    extracts questions and their repective answers an indexes from given excel sheet
    appends to individual lists
    dictionary sorts each question to respective answer
    prints dictionary, list of question, answers, and indexes on text file
    '''
    wb = pd.read_excel('extractdata.xlsx')
    rows = wb.shape[0]
    col = wb.shape[1]
    question = []
    answer = []
    for i in range(rows):
        question.append(str(wb.iloc[i,1]))
        answer.append(str(wb.iloc[i,2]))
        i += 1
    totaldata = dict(zip(question, answer))
    outJ = open("data.json", "w")
    outJ.write("{\n")
    for i in totaldata:
        outJ.write("\t[\n")
        outJ.write("\t\t" + "\"timeframe\"" + ":" + "\"2015-05\"" + ",\n")
        outJ.write("\t\t" + "\"parent\"" + ":" + "\"" + i + "\"" + ",\n")
        outJ.write("\t\t" + "\"comment\"" + ":" + "\"" + totaldata[i] + "\"" + "\n")
        outJ.write("\t],\n")
    outJ.write("}")
    outJ.close()

if __name__ == "__main__":
    dataextraction()
# outJ.write(json.dumps(totaldata, indent=4, sort_keys=True))