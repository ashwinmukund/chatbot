import sqlite3
import json
from datetime import datetime

sql_transaction = []
timeframe = '2015-05'

connection = sqlite3.connect('{}.db'.format(timeframe))
c = connection.cursor()

def create_table():
    c.execute("CREATE TABLE IF NOT EXISTS parent_reply(parent_id TEXT PRIMARY KEY, comment_id TEXT UNIQUE, parent TEXT, comment TEXT)")

def format_data(data):
    data = data.replace("\n", " newlinechar ").replace("\r", " newlinechar ").replace(' " ', " ' ")
    return data

def find_parent():
    try:
        sql = "SELECT comment FROM parent_reply WHERE comment_id = '{}' LIMIT 1".format(parent_id)
        c.execute(sql)
        results - c.fetchone(1)
        if results != None:
            return result[0]
        else:
            return False
    except Exception as e:
        #print("find_parent", e)
        return False


if __name__ == "__main__":
    create_table
    row_counter = 0
    paired_row = 0

    with open("E:/Github/actionable-science-chatbot/personal-test-2/data.json") as f:
        for row in f:
            print(row)
            row_counter += 1
            row = json.load(row)
            parent_id = row['parent_id']
            body = format_data(row['body'])
            created_utc = row['created_utc']

            parent_data = find_parent(parent_id)